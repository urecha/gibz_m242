/*
 * Blatt-12.c
 *
 * Created: 23/05/2022 08:22:23
 * Author : urecha
 */ 

#define F_CPU 3600000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <util/delay.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

void config() {
	MCUCR = 0b00001100; // Rising edge of INT 1 generates Interrupt request
	GICR = GICR | 0b11100000; //just enable all INTns
}

int main(void)
{
	DDRA = 0x00;
	DDRB = 0xFF;
	
	config();
	sei();
	while (1)
	{
	}
}

ISR(INT1_vect) {
	LEDS = ~((~LEDS)+1);
}

