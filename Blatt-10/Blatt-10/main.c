/*
 * Blatt-10.c
 *
 * Created: 09/05/2022 08:28:53
 * Author : urecha
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <util/delay.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

#define MASK 0x0F

uint8_t edge_detection(uint8_t buttons);

void config_timer() {
	TCCR0 = 0b00000100;
	TIMSK = (1 << TOIE0) | (1 << OCIE0); // == 0b00000011
	OCR0 = 8;
}

bool ledsOn = true;

int main(void)
{
	DDRA = 0x00;
	DDRB = 0xFF;
    config_timer(); 
	sei();
	
	uint8_t edges;
	
    while (1) 
    {
		edges = edge_detection(~BUTTONS);
		
		if(edges != 0x00) {
			if(edges == 0x01) {
				ledsOn = !ledsOn;	
			} else if (edges <= 0x08) {
				if((OCR0 * 2) < 255){
					//double the value ergo the time the LEDs are turned OFF, making the perceived light dimmer?
					OCR0 *= 2;
				} else {
					OCR0 = 255;
				}
			} else {
				if((OCR0 / 2 > 0)) {
					//half the value ergo the time the LEDs are turned ON, making the perceived light lighter?
					OCR0 /= 2;
				}
			}
		}
		_delay_ms(10);
    }
}

ISR(TIMER0_COMP_vect) {
	LEDS = ~0x00;	
}

ISR(TIMER0_OVF_vect) {
	if(ledsOn) {
		LEDS = ~0xFF;	
	}
}

uint8_t edge_detection(uint8_t buttons)
{
	static uint8_t previous = 0x00;
	uint8_t edges;
	
	edges = buttons & ~previous;
	
	// remember previous buttons
	previous = buttons;
	
	return edges;
}

