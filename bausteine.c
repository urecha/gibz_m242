/*
 * Bausteine.c
 * Blöcke um die Prüfungslösung zusammenzustellen oder so
 * Blatt 09-12 explizit prüfungsrelevant
 *
 * Author: urecha
 */ 

#define F_CPU 3600000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <util/delay.h>

#define BUTTONS PORTA
#define BUTTON_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

// to make stuff even clearer, define these things up here
#define PRESCALE		0x04  // Factor 256, 0b000
#define FAST_PWM		((1 << WGM01) | (1 << WGM00))
#define INVERTING_MODE	((1 << COM01) | (1 << COM00))

uint8_t edge_detection(uint8_t buttons);

void config_timer_blatt_09() {
	TCCR0 = (1 << CS02) | (1 << CS00)
	
	OCR0 = 16;
}

void config_timer_blatt_10() {
	TCCR0 = 1 << CS02;
	TIMSK = (1 << TOIE0) | (1 << OCIE0); //TIMER OVERFLOW INTERRUPT ENABLE on and OUTPUT CMP MATCH INTERRUPT ENABLE too.
	
	OCR0 = 8;
}

void config_timer_blatt_11() {
	TCCR0 = 0b01101100; // fast PWM enabled, non-inverting
	 //alternative way of writing
	TCCR0 = (1 << WGM00) | (1 << COM01) | (1 << WGM01) | (1 << CS02);
	
	OCR0 = 8;
}

void config_timer_blatt_12() {
	MCUCR = 0b00001100; // Rising edge of INT 1 generates Interrupt request
	//alternatively
	MCUCR = (1 << ISC11) | (1 << ISC10);
	
	GICR = 0b11100000; //just enable all INTns
	//or alternatively, and more precisely
	GICR = (1 << INT1);
	
	TCCR0 = (1 << WGM01) | (1 << WGM00) | (1 << COM01) | (1 << CS02); //fast PWM, clear OC0 on CMP match, prescale /256
}

bool ledsOn = true;

int main(void)
{
	//generic
	DDRA = 0x00; //output
	DDRB = 0xFF; //input
	
	//set enable interrupts
	sei();
	
	//Blatt 09
	//"Status-LED"; Set some LED to blink every second.
	#pragma region blatt_09
	config_timer_blatt_09();
	#pragma endregion
	
	//Blatt 10
	//"Dimmed LEDS"
	#pragma region blatt_10
	config_timer_blatt_10()
	
	uint8_t edges;
	
	while (1)
	{
		edges = edge_detection(~BUTTONS);
		
		if(edges != 0x00) {
			if(edges == 0x01) {
				ledsOn = !ledsOn;
				} else if (edges <= 0x08) {
				if((OCR0 * 2) < 255){
					//double the value ergo the time the LEDs are turned OFF, making the perceived light dimmer
					OCR0 *= 2;
					} else {
					OCR0 = 255;
				}
				} else {
				if((OCR0 / 2 > 0)) {
					//half the value ergo the time the LEDs are turned ON, making the perceived light lighter
					OCR0 /= 2;
				}
			}
		}
		_delay_ms(10);
	}
	
	#pragma endregion
	
	//Blatt 11
	//"PWM Dimmer"; Use the PWM mode and its output pin to dim a LED
	#pragma region blatt_11
	
	//all the code it really takes is the config
	//additionally, you need to connect the affected pins
	
	//DDRB = 0xFF works too, but this is more precise for this use-case
	//PB3 (Port B Bit 3) doubles as OC0, external output for TC0 CMP match and PWM timer function
	DDRB = 1 << PB3;
	config_timer_blatt_11();
	
	#pragma endregion
	
	//Blatt 12
	//description
	#pragma region blatt_12
	
    config_timer_blatt_12(); 
	
	#pragma endregion
	
}

ISR(INT1_vect) {
	#pragma region isr_blatt_12
	
	LEDS = ~((~LEDS)+1); //increase the counter (which is LEDS)
	
	#pragma endregion 
}

/**
* Interrupt Service Routine, runs when TIMER0 overflows
*/
ISR(TIMER0_OVF_vect) {
	//Blatt 09
	#pragma region isr_blatt_09
	
	static uint8_t counter = 0;
	counter++;
	if(counter >= 30) {
		counter = 0;
		LEDS = ~(~LEDS | 0b10000000); //activate the status LED
	} else {
		LEDS = ~(~LEDS & 0b01111111); //deactivate the status LED
	}
	
	#pragma endregion
	
	//Blatt 10
	#pragma region isr_blatt_10
	
	if(ledsOn) {
		LEDS = ~0xFF;
	}
	
	#pragma endregion
	
}

/**
* Interrupt Service Routine, runs when TIMER0's value matches OCR0's
*/
ISR(TIMER0_COMP_vect) {
	
	//Blatt 10
	#pragma region isr_blatt_10
	
	LEDS = ~0x00;
	
	#pragma endregion
}

uint8_t edge_detection(uint8_t buttons)
{
	static uint8_t previous = 0x00;
	uint8_t edges;
	
	edges = buttons & ~previous;
	
	// remember previous buttons
	previous = buttons;
	
	return edges;
}


