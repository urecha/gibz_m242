/*
 * Blatt-08.c
 *
 * Created: 28/03/2022 08:38:17
 * Author : urecha
 */ 

#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB
int main(void)
{
    DDRA = 0x00;
    DDRB = 0xFF;
    
    uint8_t bitPressed = 0x00;
	uint8_t result = 0x00;
    
    while (1)
    {
	    uint8_t currentButtons = ~BUTTONS;
	    
	    if(bitPressed != currentButtons) {
		    _delay_ms(100);
			
			//go 1 or 0 where pressed and already active or not
			uint8_t mask = bitPressed ^ currentButtons;
			result = result ^ mask;
			
			bitPressed |= currentButtons;
			bitPressed &= currentButtons;
		}
	    
	    LEDS = ~result;
    }
}


