/*
 * Blatt-02.c
 *
 * Created: 07/03/2022 08:55:45
 * Author : urecha
 */ 

#include <avr/io.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

int main(void)
{
	BUTTONS_DDR = 0x00;
	LEDS_DDR = 0xFF;
	
    while (1) 
    {
		LEDS = BUTTONS;
    }
}

