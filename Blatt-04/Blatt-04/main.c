/*
 * Blatt-04.c
 *
 * Created: 21/03/2022 08:15:14
 * Author : urecha
 */ 

#include <avr/io.h>
#include <stdint.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

#define CONFIG_MASK 0b00001111
#define VARIABLE_CONFIG_MASK = 0b11110000

#define VALUE 0xA2 // default / initial value

int main(void)
{	
	BUTTONS_DDR = 0x00;
	LEDS_DDR = 0xFF;
	
    while (1) 
    {
		LEDS = ~((BUTTONS & CONFIG_MASK) | (VALUE & VARIABLE_CONFIG_MASK));
    }
}

