/*
 * Blatt-05.c
 *
 * Created: 21/03/2022 08:42:53
 * Author : urecha
 */ 

#include <avr/io.h>
#include <stdint.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

#define INPUT_VARIABLE 0b00111010


int main(void)
{
	BUTTONS_DDR = 0x00;
	LEDS_DDR = 0xFF;
	
    while (1) 
    {
		uint8_t pressedButtonBitMask = ~BUTTONS;
		LEDS = ~(pressedButtonBitMask | INPUT_VARIABLE);
    }
}

