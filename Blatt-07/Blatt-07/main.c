/*
 * Blatt-07.c
 *
 * Created: 28/03/2022 07:56:21
 * Author : urecha
 */ 

#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

int main(void)
{
	DDRA = 0x00;
	DDRB = 0xFF; 
	
	bool toggled = false;
	bool pressing = false;
	
    while (1) 
    {
		uint8_t currentButtons = ~BUTTONS;
		uint8_t result;
		
		if(currentButtons > 0x00 && !pressing) {
			_delay_ms(100);
			pressing = true;
			toggled = !toggled;
		}
		if(currentButtons == 0x00 && pressing) {
			pressing = false;
		}
		
		if(toggled) {
			result = 0xFF;
		} else {
			result = 0x00;
		}
		
		LEDS = ~result;
    }
}

