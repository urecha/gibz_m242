/*
 * Blatt-11.c
 *
 * Created: 09/05/2022 08:28:53
 * Author : urecha
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <util/delay.h>

#define LEDS PORTB
#define LEDS_DDR DDRB

void config_timer() {
	TCCR0 = 0b01101100; // fast PWM enabled, non-inverting
	OCR0 = 8;
}

bool ledsOn = true;

int main(void)
{
	//DDRB = 1 << PB 3 ((== 0b00000100) (or any other pin) would be enough. )
	DDRB = 0xFF;
    config_timer(); 
	
    while (1) 
    {
    }
}
