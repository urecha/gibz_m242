/*
 * Blatt-06.c
 *
 * Created: 21/03/2022 08:52:26
 * Author : urecha
 */ 

#include <avr/io.h>
#include <stdint.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

#define INPUT_VARIABLE 0b10001101
//~INPUT_VARIABLE      0b01110010
//BUTTONS	(sample)   0b01111111
//					 ^=0b00001101

int main(void)
{
    BUTTONS_DDR = 0x00;
	LEDS_DDR = 0xFF;
	
    while (1) 
    {
		//default
		uint8_t CURRENT_BUTTONS = ~BUTTONS;
		
		uint8_t outputValue = CURRENT_BUTTONS ^ INPUT_VARIABLE;
		
		//default
		LEDS = ~(outputValue);
    }
}

