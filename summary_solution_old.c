

#include <avr/io.h>
#include <stdint.h>

//05
//#define INPUT 0x3A

//04
//#define INPUT 0xA2
//#define INPUT_MASK 0xF0
//#define BUTTONS_MASK 0x0F

//06
//#define INPUT 0x8D

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB

int main(void)
{
	DDRA = 0x00;
	DDRB = 0xFF;
	
	uint8_t buttons = ~BUTTONS;
	uint8_t result;
	
	//05 - display INPUT and when a button is pressed, light the corresponding LED
	//result = INPUT | buttons;
	
	//04 - display INPUT's first four bits and BUTTON's second four bits
	//result = (INPUT & INPUT_MASK) | (buttons & BUTTONS_MASK);
	
	//06 - invert led-bits where button is pressed
	//result = INPUT ^ buttons;
	//because XOR inverts wherever you got a 1 (0->1, 1->0)
	
	LEDS = ~result;
}