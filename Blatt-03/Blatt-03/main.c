/*
 * Blatt-03.c
 *
 * Created: 14/03/2022 07:52:54
 * Author : urecha
 */ 

/************************************************************************/
/* should be equivalent to cpu frequence?                                                                     */
/************************************************************************/
#define F_CPU 3600000UL

#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#define BUTTONS PINA
#define BUTTONS_DDR DDRA

#define LEDS PORTB
#define LEDS_DDR DDRB


int main(void)
{
	BUTTONS_DDR = 0x00;
	LEDS_DDR = 0xFF;
	
	int direction = 1;
	int displayValue = 0x01;
	uint8_t speed = 0x00;
	int maxSpeed = 0xFF;

	while(1) {
		speed = ~BUTTONS;
		if(speed != 0x00) {
			LEDS = ~displayValue;
			
			if(direction > 0) {
				displayValue = displayValue<<1;
			} else {
				displayValue = displayValue>>1;
			}
			
			for(int i = speed; i < maxSpeed; i = i<<1) {
				_delay_ms(100);
			}
			
			if(displayValue == 0x80 || displayValue == 0x01) direction *= -1;
		}
	}
}

